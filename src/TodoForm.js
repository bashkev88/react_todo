import React, { Component } from "react";
import { connect } from "react-redux";
import { addTodo } from "./actions";
class TodoForm extends Component {
  state = {
    text: "",
    complete: false
  };
  handleInput = e => {
    this.setState({
      text: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.newTodo(this.state);
    this.setState({
      text: ""
    });
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          value={this.state.text}
          onChange={this.handleInput}
          type="text"
          placeholder="Add todo.."
        />
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {
    todos: state
  };
};
const mapDispatchToProps = dispatch => {
  return {
    newTodo: todo => dispatch(addTodo(todo))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoForm);
