const ADD_TODO = "ADD_TODO";
const DELETE_TODO = "DELETE_TODO";
const TOGGLE_TODO = "TOGGLE_TODO";

export const addTodo = todo => {
  return {
    type: ADD_TODO,
    payload: todo
  };
};
export const toggleTodo = id => {
  return {
    type: TOGGLE_TODO,
    payload: id
  };
};
export const deleteTodo = todo => {
  return {
    type: DELETE_TODO,
    payload: todo
  };
};
