import React from "react";
import { connect } from "react-redux";
import Todo from "./Todo";

function TodoLists({ todos }) {
  return (
    <div>
      {todos.length > 0 ? (
        <ul className="collection">
          {todos.map(todo => {
            return <Todo key={todo.id} todo={todo} />;
          })}
        </ul>
      ) : (
        <p>No todos for today.</p>
      )}
    </div>
  );
}

const mapStateToProps = state => {
  return {
    todos: state
  };
};

export default connect(mapStateToProps)(TodoLists);
