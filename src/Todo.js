import React from "react";
import { connect } from "react-redux";
import { deleteTodo, toggleTodo } from "./actions";
function Todo({ todo, deleteTodo, toggleTodo }) {
  return (
    <li className="collection-item" onClick={() => toggleTodo(todo.id)}>
      <p style={{ textDecoration: todo.complete ? "line-through" : "none" }}>
        {todo.text}
      </p>
      <button onClick={() => deleteTodo(todo)}>&times;</button>
    </li>
  );
}

export default connect(
  null,
  { deleteTodo, toggleTodo }
)(Todo);
