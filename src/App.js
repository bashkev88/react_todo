import React, { Component } from "react";
import TodoForm from "./TodoForm";
import TodoList from "./TodoLists";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { reducer } from "./reducers";
// ==================== //
const store = createStore(reducer);
window.store = store;
// ==================== //
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="container">
          <TodoForm />
          <TodoList />
        </div>
      </Provider>
    );
  }
}
export default App;
