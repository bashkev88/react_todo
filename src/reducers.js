const initState = [
  { id: 1, text: "make coffee break", complete: false },
  { id: 2, text: "call to mom", complete: false }
];

export const reducer = (state = initState, action) => {
  switch (action.type) {
    case "ADD_TODO":
      return [...state, { id: Math.random() * 1111, ...action.payload }];
    case "TOGGLE_TODO":
      let toggleComplete = state.map(todo =>
        todo.id === action.payload
          ? { ...todo, complete: !todo.complete }
          : todo
      );
      return [...toggleComplete];
    case "DELETE_TODO":
      let updatedState = state.filter(t => t.id !== action.payload.id);
      return [...updatedState];
    default:
      return state;
  }
};
